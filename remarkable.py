#!/usr/bin/python
import argparse
import requests
import logging
import re
import sys, os, errno, posixpath
from urllib.parse import urlparse
from uuid import uuid4
from io import BytesIO
import zipfile
import json
from datetime import datetime, timezone

logger = logging.getLogger('remarkable')
logger.setLevel(logging.DEBUG)

console_handler = logging.StreamHandler() # sys.stderr
console_handler.setLevel(logging.INFO) # set later by set_log_level_from_verbose() in interactive sessions
console_handler.setFormatter( logging.Formatter('[%(levelname)s](%(name)s): %(message)s') )
logger.addHandler(console_handler)

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def set_log_level_from_verbose(verbose=None):
    if not verbose:
        console_handler.setLevel('ERROR')
    elif verbose == 1:
        console_handler.setLevel('WARNING')
    elif verbose == 2:
        console_handler.setLevel('INFO')
    elif verbose >= 3:
        console_handler.setLevel('DEBUG')
    else:
        console_handler.critical("UNEXPLAINED NEGATIVE COUNT!")

AUTH_API = 'https://my.remarkable.com'
SERVICE_DISCOVERY_API = 'https://service-manager-production-dot-remarkable-production.appspot.com'
STORAGE_API = 'https://document-storage-production-dot-remarkable-production.appspot.com'

TYPE_COLLECTION = 'CollectionType'
TYPE_DOCUMENT = 'DocumentType'

def listing_to_tree(listing):
    index = {x['ID']:x for x in listing}
    fullpath = {'': '/'}

    def calcpath(ID, x=None):
        if not ID in fullpath:
            if x is None:
                x = index[ID]
            fullpath[ID] = posixpath.join(calcpath(x['Parent']), x['VissibleName'])
        return fullpath[ID]

    def calcfullpath(ID):
        x = index[ID]
        parent = x['Parent']
        if x['Type'] == TYPE_DOCUMENT:
            return posixpath.join(calcpath(parent), x['VissibleName'])
        else:
            return calcpath(ID, x)

    tree = {}
    for x in listing:
        parent = calcpath(x['Parent'])
        if not parent in tree:
            tree[parent] = {}

        tree[parent][x['VissibleName']] = x

    return tree

def find_path(tree, path):
    dirname, base = os.path.split(path)
    if not dirname in tree or not base in tree[dirname]:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), path)
    return tree[dirname][base]

def create_upload_blob(ID, body):
    zipbody = BytesIO()
    with zipfile.ZipFile(zipbody, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_archive:
        zip_archive.writestr(ID + '.pdf', body.read())
        zip_archive.writestr(ID + '.pagedata', '')
        zip_archive.writestr(ID + '.content', json.dumps({
                'extraMetadata': [],
                'fileType': 'pdf',
                'lastOpenedPage': 0,
                'lineHeight': -1,
                'margins': 100,
                'pageCount': 0, # we don't know this, but it seems the reMarkable can count
                'textScale': 1,
                'transform': [] # no idea how to fill this, but it seems optional
            })
        )
    return zipbody.getvalue()

def create_directory_blob(ID):
    zipbody = BytesIO()
    with zipfile.ZipFile(zipbody, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_archive:
        zip_archive.writestr(ID + '.content', '{}')
    return zipbody.getvalue()

class RemarkableAPIError(Exception):
    pass

class Remarkable:
    def __init__(self, token):
        if token:
            self.headers = {'Authorization': "Bearer {token}".format(token=token)}
            self.refresh()
        else:
            self.headers = {'Authorization': "Bearer"}

        self.discover_services()

    def req(self, verb, url, data=None, headers={}, **kwargs):
        headers.update(self.headers)

        logger.debug('{} {}: {} {}'.format(verb, url, kwargs, headers))

        if verb == 'POST':
            r = requests.post(url, **kwargs, data=data, headers=headers)
        elif verb == 'PUT':
            r = requests.put(url, **kwargs, data=data, headers=headers)
        else:
            r = requests.get(url, **kwargs, data=data, headers=headers)

        logger.debug('Status: {} : {} bytes'.format(r.status_code, len(r.content)))
        return r

    def discover_services(self):
        params = {'environment' : 'production', 'group' : 'auth0|5a68dc51cb30df3877a1d7c4', 'apiVer' : 2 }
        r = self.req('GET', SERVICE_DISCOVERY_API + '/service/json/1/document-storage', params=params)
        data = r.json()

        logger.debug("discovery returned: {}".format(data))
        if r.status_code != 200:
            raise RemarkableAPIError("{Err}: {Msg}".format(data))

        self.storage_api = 'https://' + data['Host']
        logger.info("storage api: {}".format(self.storage_api))

    def register(self, code):
        device = str(uuid4())
        params = { 'code': code, 'deviceDesc': 'desktop-windows', 'deviceID' : device }
        r = self.req('POST', AUTH_API + '/token/json/2/device/new', json=params)
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        token = r.content.decode("utf-8")

        logger.info("register returned: {}".format(token))
        self.headers['Authorization'] = "Bearer {token}".format(token=token)
        return token

    def refresh(self):
        r = self.req('POST', AUTH_API + '/token/json/2/user/new')
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        token = r.content.decode("utf-8")

        logger.info("refresh returned: {}".format(token))
        self.headers['Authorization'] = "Bearer {token}".format(token=token)
        return token

    def list(self):
        r = self.req('GET', self.storage_api + '/document-storage/json/2/docs')
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        data = r.json()
        logger.debug("list returned: {}".format(data))
        return data

    def create_item(self, ID, name, parentID='', item_type=TYPE_DOCUMENT):
        item = {
            'ID': ID,
            'Parent': parentID,
            'Type': item_type,
            'Version': 1,
            'VissibleName': name,
            'ModifiedClient': datetime.now(timezone.utc).isoformat()
        }

        r = self.req('PUT', self.storage_api + '/document-storage/json/2/upload/update-status', json=[item])
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        item = r.json()[0]
        if not item['Success']:
            raise RemarkableAPIError(item['Message'])

        return item

    def upload_item(self, ID, name, content, parentID='', item_type=TYPE_DOCUMENT):
        stub = {'ID': ID, 'Type': item_type, 'Version' : 1}
        r = self.req('PUT', self.storage_api + '/document-storage/json/2/upload/request', json=[stub])
        logger.debug("upload request {}".format(r.content))
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        item = r.json()[0]
        if not item['Success']:
            raise RemarkableAPIError(item['Message'])

        puturl = item['BlobURLPut']
        r = self.req('PUT', puturl, data=content)#, headers={'Content-type': 'application/zip'})
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        item = self.create_item(ID, name, parentID, item_type)
        return ID

    def mkdir(self, name, parentID=''):
        ID = str(uuid4())
        zipbody = create_directory_blob(ID)
        return self.upload_item(ID, name, zipbody, parentID, TYPE_COLLECTION)

    def upload(self, name, content, parentID=''):
        ID = str(uuid4())
        zipbody = create_upload_blob(ID, content)
        return self.upload_item(ID, name, zipbody, parentID, TYPE_DOCUMENT)

    def get(self, ID, download=False):
        q = { 'doc': ID }
        if download:
            q['withBlob'] = download

        r = self.req('GET', self.storage_api + '/document-storage/json/2/docs', params=q)
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        item = r.json()[0]
        if not item['Success']:
            raise RemarkableAPIError(item['Message'])

        return item

    def delete(self, ID):
        item = self.get(ID)
        r = self.req('PUT', self.storage_api + '/document-storage/json/2/delete', json=[item])
        if r.status_code != 200:
            raise RemarkableAPIError(r.content)

        item = r.json()[0]
        if not item['Success']:
            raise RemarkableAPIError(item['Message'])

def create_filter(x):
    k,v = x.split('=')
    if v[0] == '-':
        v = re.compile(v[1:])
        return (k, lambda y: not v.match(y))
    else:
        v = re.compile(v)
        return (k, lambda y: v.match(y))

def load_token():
    try:
        with open("token", "r") as f:
            token = f.readline().rstrip('\n')
    except IOError as e:
        logging.warning("Failed to read token file: {}".format(e))
        token = None

    return token

def save_token(token):
    with open("token", "wb") as f:
        f.write(token)

def cmd_register(remarkable, code):
    token = remarkable.register(code)
    save_token(token)

def cmd_list(remarkable, filters=None):
    if filters:
        filters = dict(create_filter(x) for x in filters.split(','))

    def test_name(name):
        if not filters:
            return True

        for k,filt in filters.items():
            if not k in name:
                continue

            if not filt(name[k]):
                return False

        return True

    listing = remarkable.list()
    for index, name in enumerate(listing):
        if test_name(name):
            print(name)

def path_to_ID(remarkable, path, uuid=False):
    if not uuid:
        listing = remarkable.list()
        tree = listing_to_tree(listing)
        item = find_path(tree, path)
        ID = item['ID']
    else:
        ID = path
    return ID

def cmd_get(remarkable, path, uuid=False):
    ID = path_to_ID(remarkable, path, uuid)
    item = remarkable.get(ID)
    print(item)

def cmd_download(remarkable, path, uuid=False):
    ID = path_to_ID(remarkable, path, uuid)
    item = remarkable.get(ID, download=True)

    url = item['BlobURLGet']
    r = requests.get(url)
    with zipfile.ZipFile(BytesIO(r.content), 'r') as zip_archive:
        zip_archive.printdir()


def cmd_upload(remarkable, filename, parent='', uuid=False, name=None):
    parentID = path_to_ID(remarkable, parent, uuid=uuid)

    if name is None:
        name = os.path.basename(filename).rstrip('.pdf')

    with open(filename, "rb") as f:
        ID = remarkable.upload(name, f, parentID=parentID)
        print("file uploaded succesfully", ID)

def cmd_mkdir(remarkable, path, parent=False):
    listing = remarkable.list()
    tree = listing_to_tree(listing)

    dirname, base = posixpath.split(path)
    if parent:
        parts = dirname.split('/')

        items = tree['/']
        parentID = ''
        dirname = '/'
        for part in parts[1:]:
            if part in items:
                item = items[part]
                if item['Type'] == TYPE_DOCUMENT:
                    raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), path)

                items = tree[dirname]
            else:
                item = remarkable.mkdir(part, parentID)
                items = []

            parentID = item['ID']
            dirname = posixpath.join(dirname, part)
    else:
        if not dirname in tree:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), dirname)

        parentID = find_path(tree, dirname)['ID'] if dirname != '/' else ''
        items = tree[dirname]

    if base in items:
        raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), path)

    ID = remarkable.mkdir(base, parentID)
    print("mkdir {} succesful: {}".format(path, ID))

def cmd_delete(remarkable, path, uuid=False, yes=False):
    ID = path_to_ID(remarkable, path, uuid)
    item = remarkable.get(ID)
    print(item)

    answer = yes
    if not answer:
        answer = query_yes_no("Sure you want to delete this file?", default="no")

    if answer:
        remarkable.delete(ID)

    return answer and 0 or 1

def main(cmd, func, verbose=None, **kwargs):
    set_log_level_from_verbose(verbose)

    token = load_token()

    logger.debug("using token {}".format(token))
    logger.debug("calling {} params {}".format(cmd, kwargs))

    remarkable = Remarkable(token)
    return func(remarkable, **kwargs)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Remarkable")
    parser.add_argument('-v', '--verbose', action="count", help="verbose level... repeat up to three times.")

    subparsers = parser.add_subparsers(title='subcommands', dest="cmd", help='sub-command help')
    subparsers.required = True

    parser_register = subparsers.add_parser('register', description="Register this CLI as a new device", help="register help")
    parser_register.add_argument("code", type=str, help="The code obtained from https://my.remarkable.com/connect/desktop")
    parser_register.set_defaults(func=cmd_register)

    parser_list = subparsers.add_parser('list', description="List all the available files" ,help='list help')
    parser_list.add_argument("--filters", type=str, help="Filter files", default=None)
    parser_list.set_defaults(func=cmd_list)

    parser_get = subparsers.add_parser('get', description="Get informat of an item" ,help='get help')
    parser_get.add_argument("path", type=str, help="path or UUID of item")
    parser_get.add_argument("--uuid", help="The item is a UUID, not a path", action='store_true')
    parser_get.set_defaults(func=cmd_get)

    parser_upload = subparsers.add_parser('upload', description="Upload the given <file> to the <parent> folder", help="upload help")
    parser_upload.add_argument("filename", type=str, help="The file to upload")
    parser_upload.add_argument("--parent", type=str, help="The folder where the file should be uploaded. Empty for root (default)", default="")
    parser_upload.add_argument("--name", type=str, help="The name of the file created on the Remarkable", default=None)
    parser_upload.add_argument("--uuid", help="The parent is a UUID, not a path", action='store_true')
    parser_upload.set_defaults(func=cmd_upload)

    parser_download = subparsers.add_parser('download', description="Download an file" ,help='download help')
    parser_download.add_argument("path", type=str, help="The path or UUID of file to download")
    parser_download.add_argument("--uuid", help="The item is a UUID, not a path", action='store_true')
    parser_download.set_defaults(func=cmd_download)

    parser_mkdir = subparsers.add_parser('mkdir', description='Create a new folder hierarchy', help='mkdir help')
    parser_mkdir.add_argument("path", type=str, help="The folders using Unix notation (slashes as path separators)")
    parser_mkdir.add_argument("-p", help='no error if existing, make parent directories as needed', action='store_true', dest='parent')
    parser_mkdir.set_defaults(func=cmd_mkdir)

    parser_delete = subparsers.add_parser('delete', description="Delete a file", help="delete help")
    parser_delete.add_argument("path", type=str, help="The path or UUID of the file to delete")
    parser_delete.add_argument("--uuid", help="The item is a UUID, not a path", action='store_true')
    parser_delete.add_argument("--yes", action='store_true', help="Answer yes on questions")
    parser_delete.set_defaults(func=cmd_delete)

    options = parser.parse_args()
    try:
        ret = main(**vars(options))
        sys.exit( ret )
    except IOError as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    except (RemarkableAPIError, Exception) as e:
        print(e, file=sys.stderr)
        import traceback
        traceback.print_exc(file=sys.stderr)
        sys.exit(1)
